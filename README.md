# Chippydootles KiCad Libraries

Some personal libraries for chippydootles related things.

Components: Parts used on boards
Modules: Chippydootles Breakout Modules

Components contain some modified files with original sources of legal but questionable licensing origin (Manufacture or 3rd party) So these files are being presented as is.
